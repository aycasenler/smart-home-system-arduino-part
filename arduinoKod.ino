#include <Servo.h>
#include <dht11.h>

#include <SoftwareSerial.h>
#define garagePin 2
#define doorPin 3
#define windowPin 4
#define curtainPin 500
#define lampPin 7

#define cooler 6
#define DHT11PIN 8
#define BUZZER_PIN  12

int ledPin1 = 52;
int ledPin2 = 53;
int sensorValue;
int waterPin = A3;
int chk;
const int stepPin = 40;
const int dirPin = 39; 
int sensorHareket = 34;
int stateHareket = LOW; 
int valHareket = 0;
int val = 0;

//5 21 çalışmıyor
Servo garageServo;
Servo doorServo;
Servo windowServo;
dht11 DHT11;

void setup() {
  pinMode(lampPin, OUTPUT);
  pinMode(cooler, OUTPUT);
  pinMode(BUZZER_PIN, OUTPUT);
 
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
  pinMode(sensorHareket, INPUT);
  pinMode(ledPin1, OUTPUT);
  pinMode(ledPin2, OUTPUT);

  sensorValue = analogRead(0);
  
  chk = DHT11.read(DHT11PIN);
  garageServo.attach(garagePin);
  doorServo.attach(doorPin);
  windowServo.attach(windowPin);
  Serial.begin(115200);
  Serial2.begin(9600);

  closeDoor();
  closeGarage();
  closeWindow();
}

void loop() {
 
 
  Serial.println("Nem: %"+ String((float)DHT11.humidity));
  Serial.print("Temperature: ");
  Serial.println("Temperature: " + String((float)DHT11.temperature)+"°C");
   

  gasSensor();
  motionSensor();


  if(val == 1){
    floodSensor();
  }
  if(val == 2){
    rainSensor();
  }
  

  char ch = Serial2.read();
  switch (ch) {
    case '!': coolerOpen(); break;
    case '#': coolerClose(); break;
    case '$': openGarage(); break;
    case '%': closeGarage(); break;
    case '&': openDoor(); break;
    case '*': closeDoor(); break;
    case '+': openWindow(); break;
    case '-': closeWindow(); break;
    case '<': lampUp(); break;
    case '>': lampDown(); break;
    case '/': openCurtain(); break;
    case '?': closeCurtain(); break;
    case 'A': otoCooler(20.00); break;
    case 'B': otoCooler(21.00); break;
    case 'C': otoCooler(22.00); break;
    case 'D': otoCooler(23.00); break;
    case 'E': otoCooler(24.00); break;
    case 'F': otoCooler(25.00); break;
    case 'G': otoCooler(26.00); break;
    case 'H': otoCooler(27.00); break;
    case 'I': otoCooler(28.00); break;
    case 'J': otoCooler(29.00); break;
    case 'K': otoCooler(30.00); break;
    case 'L': floodSensor(); val =1; break;
    case 'M': rainSensor(); val =2; break;
  }

}
void openGarage() {
  garageServo.write(90);
}

void closeGarage() {
  garageServo.write(0);
}

void openDoor() {
  doorServo.write(80);
}

void closeDoor() {
  doorServo.write(0);
}
void openWindow() {
  windowServo.write(0);
}


void closeWindow() {
  windowServo.write(70);
}


void lampUp() {
  digitalWrite(lampPin, HIGH);
}

void lampDown() {
  digitalWrite(lampPin, LOW);
}
void coolerOpen() {
  digitalWrite(cooler, HIGH);

}
void coolerClose() {
  digitalWrite(cooler, LOW);
}
void gasSensor() {
  sensorValue = analogRead(0);       
  Serial.println("Gas Level: " + String(sensorValue, DEC));
  delay(500);
  if (sensorValue > 400) {
    digitalWrite(BUZZER_PIN, HIGH);
  }
  else {
    digitalWrite(BUZZER_PIN, LOW);
    //delay(500);
  }
}
void openCurtain() {
  
  digitalWrite(dirPin, HIGH); 
  for (int x = 0; x < 500; x++) {
    digitalWrite(stepPin, HIGH);
     delayMicroseconds(500);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(500);
  }

}
void closeCurtain() {

  digitalWrite(dirPin, LOW); 
  for (int x = 0; x < 500; x++) {
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(500);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(500);
  }
}
void motionSensor(){
  valHareket = digitalRead(sensorHareket); 
if (valHareket == HIGH) {
digitalWrite(ledPin1, HIGH); 
digitalWrite(ledPin2, HIGH);
Serial.println("Motion Detected");
delay(100);
  
if (stateHareket == LOW) {

stateHareket = HIGH;
}
}
else {
digitalWrite(ledPin1, LOW); // LED i kapat
digitalWrite(ledPin2, LOW);
delay(200);
  
if (stateHareket == HIGH){
Serial.println("Motion Undetected");

stateHareket = LOW;
}
}
}
void floodSensor(){
  int value = analogRead(waterPin); 
   
      Serial.println("Water level: " + String(value));
   
      if (value > 280)
      {
       tone(BUZZER_PIN, 1000,100);
      } 
}
void rainSensor(){
  int value = analogRead(waterPin);
      
      if (value > 280)
      {
       closeDoor();
       closeWindow();
       closeGarage();
      } 
}
void otoCooler(float value){
 
  if ((float)DHT11.temperature > value){
    coolerOpen();
  }
  else{
  coolerClose();
  }
}
